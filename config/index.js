'use strict';

const debug = require('debug')('dracade:config');

/**
 * Simple configuration handler
 */
class Configuration {

  /**
   * Get a value from an object specified within a file.
   *
   * @param {string} path
   * @param {string} defaultValue
   * @returns value
   */
  static get(path, defaultValue) {
    var pathSplit = path.split('.');
    var value = null;
    try {
      var file = require('./' + pathSplit[0]);
      value = file;
      for (let i = 1; i < pathSplit.length; i++) {
        var exists = false;
        if (i === 1) {
          if (pathSplit[i] != null) {
            value = file[pathSplit[i]];
            exists = true;
          }
        } else {
          if (value != null) {
            if (pathSplit[i] != null) {
              value = value[pathSplit[i]];
              exists = true;
            }
          }
        }

        if (!exists) {
          debug('\'' + pathSplit[0] + '\' doesn\'t exist within \'' + path + '\'.');
          break;
        }
      }
    } catch(e) {
      debug('Unable to find a file with the name \'' + pathSplit[0] + '.js\'');
    }

    if (value && typeof value === 'string') {
      value = (value.toLowerCase() === 'true' || value.toLowerCase() === 'false') ? (value.toLowerCase() === 'true' ? true : false) : value;
    }

    return (value !== undefined && value !== null) ? value : defaultValue;
  }

  /**
   * Check if the specified path exists.
   *
   * @param {string} path
   * @returns {boolean}
   */
  static has(path) {
    return (this.get(path) !== null && this.get(path) !== undefined);
  }
}

module.exports = Configuration;
