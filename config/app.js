'use strict';

/**
 * Application configuration
 */
let options = {};

/**
 * Prettify response output
 */
options['prettify'] = true;

/**
 * Application port
 */
options['port'] = process.env.APP_PORT || 80;

/**
 * Application key
 */
options['key'] = process.env.APP_KEY != null ? process.env.APP_KEY : '_dracade';

/**
 * Run the application using a HTTPS connection
 */
options['secure'] = process.env.APP_SECURE != null ? process.env.APP_SECURE : true;

/**
 * Run the application behind a proxy server
 */
options['behindProxy'] = process.env.APP_BEHIND_PROXY != null ? process.env.APP_BEHIND_PROXY : false;

module.exports = options;
