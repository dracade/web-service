'use strict';

/**
 * Database configuration
 */
let options = {};

/**
 * Database's host
 *
 * The database's host address.
 */
options['host'] = process.env.DB_HOST || '127.0.0.1';

/**
 * Database's port
 *
 * The database's port.
 */
options['port'] = process.env.DB_PORT || 3306;

/**
 * Database's dialect
 *
 * The dialect of the SQL database.
 * Supports:
 *
 *    mysql
 *    mariadb
 *    sqlite
 *    postgres
 *    mssql
 */
options['dialect'] = process.env.DB_DIALECT || 'mysql';

/**
 * Database's storage
 *
 * If the sqlite dialect was selected then
 * it's contents will be stored in the specified
 * relative path.
 */
options['storage'] = '../storage/database.sqlite';

/**
 * Database's username
 *
 * The username used to access the database.
 */
options['username'] = process.env.DB_USERNAME || 'root';

/**
 * Database's password
 *
 * The password used to access the database.
 */
options['password'] = process.env.DB_PASSWORD || null;

/**
 * Database's name
 *
 * The name of the database to access.
 */
options['database'] = process.env.DB_NAME || 'dracade';

/**
 * Database table prefix
 *
 * The prefix that is assigned to each table.
 */
options['tablePrefix'] = process.env.DB_TABLE_PREFIX || '';

/**
 * Database's logging
 *
 * Log any database errors to the console.
 */
options['logging'] = false;

module.exports = options;
