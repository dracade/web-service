'use strict';

const _ = require('lodash');
const events = require('events');
const util = require('util');
const debug = require('debug')('dracade:api:battleground:cagematch');
const models = require('../../models');
const responses = require('../../responses');

/**
 * Battleground: CageMatch api
 */
function CageMatch() {
  var self = this;

  /**
   * Browse through all of the records.
   */
  self.browse = function(options) {
    return new Promise(function(resolve, reject) {
      models.CageMatch.findAndCountAll(options).then(function(data) {
        resolve({
          meta: {
            count: data.count,
            limit: options.limit,
            offset: options.offset
          },
          data: data.rows
        });
      }).catch(function(error) {
        debug(error);
        reject(new responses.InternalServerError);
      });
    });
  };

  /**
   * Find a single record.
   */
  self.find = function(options) {
    return new Promise(function(resolve, reject) {
      var param = options.playerId;
      var query = _.extend(options, {
          'where': {
          '$or': [
            { 'uniqueId': param }
          ]
        }
      });

      models.CageMatch.findOne(query).then(function(data) {
        resolve({
          meta: {
            limit: options.limit,
            offset: options.offset
          },
          data: data
        });
      }).catch(function(error) {
        debug(error);
        reject(new responses.InternalServerError);
      })
    });
  };

  events.EventEmitter.call(this);
}

util.inherits(CageMatch, events.EventEmitter);
module.exports = new CageMatch();
