'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const config = require('../config');
const sequelize = new Sequelize(config.get('database.database'), config.get('database.username'), config.get('database.password'), config.get('database'));

var db = {};

/**
 * Retrieve models from a specific directory.
 */
db.loadFromDirectory = function(directory) {
  fs.readdirSync(directory).filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== 'index.js');
  }).forEach(function(file) {
    var p = path.join(directory, file);
    if (fs.lstatSync(p).isDirectory()) {
      db.loadFromDirectory(p);
    } else {
      var model = sequelize.import(p);
      db[model.name] = model;
    }
  });
};

/**
 * Associate the models with their respected relationships.
 */
db.associate = function() {
  Object.keys(db).forEach(function(modelName) {
    if ('associate' in db[modelName]) {
      db[modelName].associate(db);
    }
  });
};

/**
 * Modify table names before definition
 */
sequelize.addHook('beforeDefine', function(attributes, options) {
  if (options.allowTablePrefix !== false) {
    options.tableName = config.get('database.tablePrefix') + options.tableName;
  }
});

/**
 * Autoload sequelize models
 */
db.loadFromDirectory(__dirname);
db.associate();

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
