'use strict';

module.exports = function(sequelize, dataTypes) {
  return sequelize.define('FreeForAll', {
    uniqueId: {
      type: dataTypes.UUID,
      allowNull: false,
      unique: true
    },
    totalKills: {
      type: dataTypes.INTEGER,
      allowNull: false,
      index: true
    },
    totalDeaths: {
      type: dataTypes.INTEGER,
      allowNull: false,
      index: true
    },
    totalTimePlayed: {
      type: dataTypes.BIGINT,
      allowNull: false
    }
  }, {
    timestamps: false,
    tableName: 'battleground_freeforall_statistics',
    allowTablePrefix: false,
    instanceMethods: {
      /**
       * Override default toJSON function.
       *
       * @returns {Object} json
       */
      toJSON: function() {
        var values = this.get();

        return values;
      }
    }
  });
};
