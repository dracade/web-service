'use strict';

module.exports = function(sequelize, dataTypes) {
  return sequelize.define('CageMatch', {
    uniqueId: {
      type: dataTypes.UUID,
      allowNull: false,
      unique: true
    },
    rating: {
      type: dataTypes.DOUBLE,
      allowNull: false
    },
    rankedKills: {
      type: dataTypes.INTEGER,
      allowNull: false
    },
    rankedDeaths: {
      type: dataTypes.INTEGER,
      allowNull: false
    },
    rankedWins: {
      type: dataTypes.INTEGER,
      allowNull: false
    },
    rankedLoses: {
      type: dataTypes.INTEGER,
      allowNull: false
    },
    rankedDraws: {
      type: dataTypes.INTEGER,
      allowNull: false
    },
    unrankedKills: {
      type: dataTypes.INTEGER,
      allowNull: false
    },
    unrankedDeaths: {
      type: dataTypes.INTEGER,
      allowNull: false
    },
    unrankedWins: {
      type: dataTypes.INTEGER,
      allowNull: false
    },
    unrankedLoses: {
      type: dataTypes.INTEGER,
      allowNull: false
    },
    unrankedDraws: {
      type: dataTypes.INTEGER,
      allowNull: false
    },
    totalTimePlayed: {
      type: dataTypes.BIGINT,
      allowNull: false
    }
  }, {
    timestamps: false,
    tableName: 'battleground_cagematch_statistics',
    allowTablePrefix: false,
    instanceMethods: {
      /**
       * Override default toJSON function.
       *
       * @returns {Object} json
       */
      toJSON: function() {
        var values = this.get();

        return values;
      }
    }
  });
};
