'use strict';

/**
 * Create an UnauthorizedError response.
 *
 * @returns {Object} error
 */
function UnauthorizedError() {
  var self = this;

  self.type = this.name;
  self.status = 401;
  self.exceptions = [];

  self.exception = function(exception) {
    self.exceptions.push(exception);
    return self;
  };
}

UnauthorizedError.prototype = Object.create(require('./base-error').prototype);
UnauthorizedError.prototype.name = 'UnauthorizedError';

module.exports = UnauthorizedError;
