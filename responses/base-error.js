'use strict';

/**
 * Create a base error.
 *
 * @returns {Object} error
 */
function BaseError() {
  var self = this;

  self.type = this.name;
  self.stack = new Error().stack;
}

BaseError.prototype = Object.create(Error.prototype);
BaseError.prototype.name = 'BaseError';

module.exports = BaseError;
