'use strict';

/**
 * Create a ValidationError response.
 *
 * @returns {Object} error
 */
function ValidationError() {
  var self = this;

  self.type = this.name;
  self.status = 422;
  self.exceptions = [];

  self.exception = function(exception) {
    self.exceptions.push(exception);
    return self;
  };
}

ValidationError.prototype = Object.create(require('./base-error').prototype);
ValidationError.prototype.name = 'ValidationError';

module.exports = ValidationError;
