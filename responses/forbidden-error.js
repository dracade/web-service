'use strict';

/**
 * Create a ForbiddenError response.
 *
 * @returns {Object} error
 */
function ForbiddenError() {
  var self = this;

  self.type = this.name;
  self.status = 403;
  self.exceptions = [];

  self.exception = function(exception) {
    self.exceptions.push(exception);
    return self;
  };
}

ForbiddenError.prototype = Object.create(require('./base-error').prototype);
ForbiddenError.prototype.name = 'ForbiddenError';

module.exports = ForbiddenError;
