'use strict';

const debug = require('debug')('dracade:responses');

module.exports = {
  'BaseError': require('./base-error'),
  'InternalServerError': require('./internal-server-error'),
  'NotFoundError': require('./not-found-error'),
  'BadRequestError': require('./bad-request-error'),
  'ValidationError': require('./validation-error'),
  'UnauthorizedError': require('./unauthorized-error'),
  'ForbiddenError': require('./forbidden-error')
};

/**
 * Route error response handler
 */
module.exports.handler = function(error, request, response, next) {
  if (!error.status) {
    debug(error);
    error = new module.exports.InternalServerError;
  }
  var status = error.status || 500;

  if (process.env.NODE_ENV.toLowerCase() === 'production' || process.env.NODE_ENV.toLowerCase() === 'testing') {
    delete error.stack;
  }

  response.status(status).json({
    'error': error
  });
};
