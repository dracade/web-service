'use strict';

const express = require('express');
const api = require('../api');
const middleware = require('../middleware').named;
const config = require('../config');
const models = require('../models');

var router = express.Router();

router.get('/minigames/battleground/freeforall', api.http(api.battleground.freeforall.browse));
router.get('/minigames/battleground/freeforall/:playerId', api.http(api.battleground.freeforall.find));

router.get('/minigames/battleground/cagematch', api.http(api.battleground.cagematch.browse));
router.get('/minigames/battleground/cagematch/:playerId', api.http(api.battleground.cagematch.find));

/**
 * Export the router
 */
module.exports = router;
